//
// Created by Эльдар Янбеков on 12.12.2022.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_INNER_IMG_H
#define ASSIGNMENT_3_IMAGE_ROTATION_INNER_IMG_H

#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(uint64_t width , uint64_t height);

void destroy_image(struct image * img);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_INNER_IMG_H
