//
// Created by Эльдар Янбеков on 12.12.2022.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMP_INTERPRET_H
#define ASSIGNMENT_3_IMAGE_ROTATION_BMP_INTERPRET_H

#include "bmp_header.h"
#include "inner_img.h"
#include <stdio.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_MALLOC_FAIL,
    READ_NULL_ARGS,
    READ_FAIL
    /* коды других ошибок  */
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_NULL_ARGS
    /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );



#endif //ASSIGNMENT_3_IMAGE_ROTATION_BMP_INTERPRET_H
