//
// Created by Эльдар Янбеков on 24.01.2023.
//

#ifndef IMAGE_TRANSFORMER_FILE_HANDLER_H
#define IMAGE_TRANSFORMER_FILE_HANDLER_H
#include "bmp_interpret.h"
#include "image_transformation.h"
#include "inner_img.h"

bool read_bmp_file(const char* filename, struct image* img);
bool write_bmp_file(const char* filename, struct image* img);

#endif //IMAGE_TRANSFORMER_FILE_HANDLER_H
