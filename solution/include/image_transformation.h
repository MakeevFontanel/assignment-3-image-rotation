//
// Created by Эльдар Янбеков on 12.12.2022.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_TRANSFORMATION_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_TRANSFORMATION_H
#include "bmp_interpret.h"
#include <stdbool.h>


void image_rotate90deg(struct image* img );

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_TRANSFORMATION_H
