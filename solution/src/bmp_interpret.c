//
// Created by Эльдар Янбеков on 12.12.2022.
//
#include "bmp_interpret.h"
#include "inner_img.h"
#include <stdio.h>
#include <stdlib.h>

enum {
    BMP_BF_TYPE = 0x4d42,
    BMP_BI_BIT_COUNT = 24,
    BMP_BF_RESERVED = 0,
    BMP_BI_SIZE = 40,
    BMP_BI_PLANES = 1,
    BMP_BI_COMPRESSION = 0,
    BMP_BI_X_PELS_PER_METER = 0,
    BMP_BI_Y_PELS_PER_METER = 0,
    BMP_BI_CLR_USED = 0,
    BMP_BI_CLR_IMPORTANT = 0,
    BMP_PIXEL_SIZE = sizeof(struct pixel),
    BMP_HEADER_SIZE = sizeof(struct bmp_header)
};

enum read_status from_bmp( FILE* in, struct image* img ) {
    if (!in || !img) return READ_NULL_ARGS;

    struct bmp_header *header = malloc (BMP_HEADER_SIZE);
    if (header == NULL) return READ_MALLOC_FAIL;

    if (!fread(header , BMP_HEADER_SIZE , 1 , in))
        return READ_INVALID_HEADER;
    if (header->biBitCount != BMP_BI_BIT_COUNT)
        return READ_INVALID_BITS;
    if (header->bfType != BMP_BF_TYPE)
        return READ_INVALID_SIGNATURE;

    uint32_t height = header -> biHeight;
    uint32_t width = header -> biWidth;

    *img = create_image(header -> biWidth , header -> biHeight);
    if (img->data == NULL)
        return READ_MALLOC_FAIL;

    for (size_t i =0; i < height; i++) {
        if (!fread((img->data) + i*width, BMP_PIXEL_SIZE , width , in ))
            return READ_FAIL;
        if (((BMP_PIXEL_SIZE*width) % 4)) {
            if(fseek(in, 4 - (BMP_PIXEL_SIZE*width) % 4,SEEK_CUR))
                return READ_FAIL;
        }
    }
    free(header);
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    if (!out || !img) return WRITE_NULL_ARGS;

    size_t width = img -> width;
    size_t height = img -> height;
    struct bmp_header bmh = {
        BMP_BF_TYPE,
        height * width * BMP_PIXEL_SIZE + BMP_HEADER_SIZE,
        BMP_BF_RESERVED,
        BMP_HEADER_SIZE,
        BMP_BI_SIZE,
        width,
        height,
        BMP_BI_PLANES,
        BMP_BI_BIT_COUNT,
        BMP_BI_COMPRESSION,
        BMP_PIXEL_SIZE,
        BMP_BI_X_PELS_PER_METER,
        BMP_BI_Y_PELS_PER_METER,
        BMP_BI_CLR_USED,
        BMP_BI_CLR_IMPORTANT
    };

    if (fwrite(&bmh , sizeof bmh, 1 ,out) != 1) {
        return WRITE_ERROR;
    }

    //write image with padding
    for (size_t i = 0; i < height; i++) {
        if(!fwrite((img->data) + i*width, BMP_PIXEL_SIZE*width, 1, out))
            return WRITE_ERROR;
        if ((BMP_PIXEL_SIZE*width) % 4)
            if(!fwrite(img->data, 4 - (BMP_PIXEL_SIZE*width) % 4, 1, out))
                return WRITE_ERROR;
    }
    return WRITE_OK;
}
