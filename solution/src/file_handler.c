//
// Created by Эльдар Янбеков on 23.01.2023.
//
#include "file_handler.h"


bool read_bmp_file(const char* filename, struct image* img) {
    FILE *in = fopen(filename, "rb");
    if(from_bmp(in, img)) {
        fclose(in);
        return false;
    }
    fclose(in);
    return true;
}

bool write_bmp_file(const char* filename, struct image* img) {
    FILE *out = fopen(filename, "wb");
    if(to_bmp(out, img)) {
        fclose(out);
        return false;
    }
    fclose(out);
    return true;
}
