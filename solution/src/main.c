#include "file_handler.h"

#include "stdio.h"
int main( int argc, char** argv ) {
    if (argc < 3)
        return 1;
    struct image img;
    if (!read_bmp_file(argv[1], &img)) {
        return 1;
    }
    image_rotate90deg(&img);
    if (!write_bmp_file(argv[2], &img)) {
        return 1;
    }
    destroy_image(&img);
    return 0;
}
