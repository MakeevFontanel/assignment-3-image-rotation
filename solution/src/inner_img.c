//
// Created by Эльдар Янбеков on 12.12.2022.
//
#include "inner_img.h"

#include <stdlib.h>


struct image create_image(const uint64_t width , const uint64_t height) {
    return (struct image) {.height = height ,
            .width = width ,
            .data = malloc(sizeof (struct pixel) * width * height)};
}

void destroy_image(struct image * img) {
    if (img == NULL)
        return;
    if (img->data == NULL)
        return;
    free(img->data);
}
