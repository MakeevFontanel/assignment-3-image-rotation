//
// Created by Эльдар Янбеков on 12.12.2022.
//

#include "image_transformation.h"
#include "inner_img.h"
#include <stdlib.h>


void swap (uint64_t* a , uint64_t* b) {
    if (a != b)
    {
        *a ^= *b;
        *b ^= *a;
        *a ^= *b;
    }
}

void image_rotate90deg(struct image* img ) {
    if(img == NULL)
        return;

    struct pixel* data_rotated = malloc(sizeof(struct pixel) * (img -> height) * (img -> width));
    if (data_rotated == NULL)
        return;

    swap(&(img -> height) , &(img -> width));
    for (size_t i = 0 ; i < (img -> height); i++) {
        for (size_t j = 0; j < (img->width); j++) {
            data_rotated[i * ((img->width) )+ j ] = (img->data)[((img->width) -1- j) * ((img->height)) + i];
        }
    }
    free(img -> data);
    img -> data = data_rotated;
}
